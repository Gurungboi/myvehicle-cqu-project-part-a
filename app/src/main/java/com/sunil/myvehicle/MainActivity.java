package com.sunil.myvehicle;


import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.Toast;
import java.util.ArrayList;
import java.util.List;


/**
 * Created by Sunil Gurung on 14/12/2018.
 */



public class MainActivity extends AppCompatActivity {

    //current page index
    private int pageIndex = 5;
    public static List<VehicleLog> vehicleLog_List = new ArrayList<>();
    DatabaseHelper db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        setSupportActionBar(toolbar);
        db = new DatabaseHelper(this);
        CurrentPage();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement

        switch (item.getItemId()) {
            case R.id.btnSendMenu:
                //send


                AlertDialog.Builder builderSend = new AlertDialog.Builder(this);
                builderSend.setIcon(R.mipmap.ic_launcher);
                //builderSend.setTitle("Confirm");
                builderSend.setTitle("Are you sure? This will delete all Entries");

                builderSend.setPositiveButton("YES", new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {
                        // Do nothing but close the dialog
                        vehicleLog_List.clear();
                        db.delete();
                        db.close();
                        Toast.makeText(getApplicationContext(), "Deleted Successfully",
                                Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(getApplicationContext(),MainActivity.class);
                        startActivity(intent);


                    }
                });

                builderSend.setNegativeButton("NO", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        // Do nothing
                        dialog.dismiss();
                    }
                });

                AlertDialog alertSend = builderSend.create();
                alertSend.show();
                return true;

            case R.id.btnSaveMenu:
                //save to database
                //save

                AlertDialog.Builder builderSave = new AlertDialog.Builder(this);
                builderSave.setIcon(R.mipmap.ic_launcher);

                //builderSave.setTitle("Confirm");
                builderSave.setTitle("Save entries to DB First?");

                builderSave.setPositiveButton("YES", new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {
                        // Do nothing but close the dialog
                        if (insertDB()) {
                            Toast.makeText(getApplicationContext(), "Data Successfully Inserted!", Toast.LENGTH_LONG).show();
                        }
                        dialog.dismiss();

                    }
                });

                builderSave.setNegativeButton("NO", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        // Do nothing
                        Toast.makeText(getApplicationContext(), "Data didn't Save Successfully!", Toast.LENGTH_LONG).show();

                        dialog.dismiss();

                    }
                });

                AlertDialog alertSave = builderSave.create();
                alertSave.show();

                return true;
            case R.id.btnProfileMenu:
                ProfileFragment profileFragment = new ProfileFragment();
                FragmentManager manager = getSupportFragmentManager();
                FragmentTransaction transaction = manager.beginTransaction();
                transaction.replace(R.id.mainActivity, profileFragment,"profileFragment");
                transaction.addToBackStack("profileFragment");
                transaction.commit();
                return true;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * It will return back to the previous page
     * @param view
     */


    public void returnToVehiclePage(View view) {
        VehicleFragment vehicle_fragment = new VehicleFragment();
        Bundle args = new Bundle();
        args.putInt("vehicle", pageIndex);

        vehicle_fragment.setArguments(args);
        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.replace(R.id.mainActivity, vehicle_fragment,"vehicle_fragment");
        transaction.addToBackStack("vehicle_fragment");
        transaction.commit();

    }

    /**
     * Show the current visiting page
     */
    public void CurrentPage() {

        Fragment frag = new Fragment();
        if (pageIndex == 5) {
            frag = new HomeFragment();
        }
        if (pageIndex >= 0 && pageIndex <= 4) {
            frag = new VehicleFragment();
            Bundle args = new Bundle();
            args.putInt("vehicle", pageIndex);
            frag.setArguments(args);
        }
        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();

        transaction.replace(R.id.mainActivity, frag);
        transaction.commit();
    }

    /**
     * it will change the page
     * using page index
     * as it will be incremental
     * @param view
     */
    public void NextPage(View view) {
        if (pageIndex == 4) {
            pageIndex = 0;
        } else {
            pageIndex++;
        }
        CurrentPage();
    }

    /**
     * shows previous vehicle page
     */
    public void PreviousPage(View view) {
        //check for shed index and cyclic
        if (pageIndex == 0) {
            pageIndex = 4;
        } else {
            pageIndex--;
        }
        CurrentPage();
    }

    /**
     * show home page
     */
    public void HomePage(View view) {
        Home();
    }

    /**
     * Create an object to reference the page
     *
     */

    public void Home() {
        pageIndex = 5;
        CurrentPage();
    }

    /**
     * Get the  pageindex
     * It will chnage the page according to the pageIndex
     * @param view
     */
    public void vehicleEntry(View view) {
        int intID = view.getId();
        Button button = (Button) findViewById(intID);
        pageIndex = Integer.valueOf((String) button.getTag());
        //changing view
        VehicleFragment vehiclefragment = new VehicleFragment();
        Bundle args = new Bundle();
        args.putInt("vehicle", pageIndex);
        vehiclefragment.setArguments(args);

        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.replace(R.id.mainActivity, vehiclefragment,"vehicle_fragment");
        transaction.addToBackStack("vehicle_fragment");
        transaction.commit();

    }

    @Override
    public void onBackPressed() {
        new AlertDialog.Builder(this)
                .setTitle(R.string.exit_message_title)
                .setMessage("Save entry to database first?")
                .setNegativeButton(android.R.string.no, null)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {
                        insertDB();
                        setResult(RESULT_OK, new Intent().putExtra("EXIT", true));
                        finish();
                    }

                }).create().show();
    }

    /**
     * saving data
     *
     * @return
     */
    public boolean insertDB() {
        try {
            for (VehicleLog v : vehicleLog_List) {
                db.insertVehicleLog(v);
            }
            //vehicleLog_List.clear();
            return true;
        } catch (Exception ignored) {
        }
        return false;
    }

    public static ArrayList<VehicleLog> filterdList(int vehicleTag) {
        ArrayList<VehicleLog> result = new ArrayList<>();
        for (VehicleLog vehicleLog : vehicleLog_List) {
            if (vehicleLog.getVehicleList() == vehicleTag) {
                result.add(vehicleLog);
            }
        }

        return result;

    }
}
