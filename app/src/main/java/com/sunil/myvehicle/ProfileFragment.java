package com.sunil.myvehicle;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

/**
 * Created by Sunil Gurung on 14/12/2018.
 */

public class ProfileFragment extends Fragment {

   //DECLARE VARIABLES
    private Button btnSave;
    private Button btnCancel;
    private EditText txtLoginUsername;
    private EditText txtLoginPassword;
    private EditText txtLoginRepeatPassword;


    /**
     *   Required empty public constructor
     */
    public ProfileFragment() {
    }

    /**
     *
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return
     */

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.profile_fragment, container, false);
        btnSave = (Button) view.findViewById(R.id.btnSave);
        btnCancel = (Button) view.findViewById(R.id.btnCancel);

        txtLoginUsername = (EditText) view.findViewById(R.id.txtLoginUsername);
        txtLoginPassword = (EditText) view.findViewById(R.id.txtLoginPassword);
        txtLoginRepeatPassword = (EditText) view.findViewById(R.id.txtLoginRepeatPassword);


        //SAVE THE DETAILS

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String username = txtLoginUsername.getText().toString();
                String password = txtLoginPassword.getText().toString();
                String repeatconfirmPassword = txtLoginRepeatPassword.getText().toString();

                if (!emptyValidation()){
                    DatabaseHelper db = new DatabaseHelper(getActivity());

                    if (db.createUser(new User(username, password,repeatconfirmPassword)) != null) {
                        Toast.makeText(getContext(), "Added User", Toast.LENGTH_SHORT).show();
                        ((MainActivity) getActivity()).Home();

                    }else{
                        Toast.makeText(getContext(), "User Not Addded", Toast.LENGTH_SHORT).show();
                    }
                }
                else
                    Toast.makeText(getContext(), "Entry not saved as not all data entered. Complete all entries and try again", Toast.LENGTH_SHORT).show();

            }
        });


        //CONDITION WILL GO BACK TO HOMEFRAGMENT

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity) getActivity()).Home();
            }
        });
        return view;
    }


    //Checks the edittext is empty or not

    private boolean emptyValidation() {
        if (TextUtils.isEmpty(txtLoginUsername.getText().toString()) || TextUtils.isEmpty(txtLoginPassword.getText().toString()) || TextUtils.isEmpty(txtLoginRepeatPassword.getText().toString())) {
            return true;
        }
        return false;

    }
}
